import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {useState, useEffect} from 'react';
import AppNavbar from './components/Navbar';
import Footer from './components/Footer';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Commission from './pages/Commission';
import Contact from './pages/Contacts';
import TOS from './pages/TOS';
import Dashboard from './pages/Dashboard';
import Cart from './pages/Cart';
import AddToCart from './pages/AddToCart';
import {UserProvider} from './UserContext';


function App() {

  const [user, setUser] = useState({id: null, email: null, isAdmin: false});

  const unSetUser = () =>{
       localStorage.removeItem("token");
       setUser({id: null, isAdmin: false});
  }

  useEffect(() =>{
    if(localStorage.getItem("token") !== null){
      fetch(`${process.env.REACT_APP_URI}/users/getUserDetail`,
        {headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }}).then(response => response.json())
      .then(data => {
        setUser({id:data._id, email:data.email, isAdmin:data.isAdmin});
      });
    }
  }, []);

  return (
    <UserProvider value = {{user,setUser,unSetUser}}>  
      <Router>
          <AppNavbar/>
          {
            (user.isAdmin)?
            <Routes>
            <Route path = "*" element = {<Dashboard/>}/>
            <Route path = "/logout" element = {<Logout/>}/>
            </Routes>
            :
            (localStorage.getItem("token") === null)?
              <Routes>
              <Route path = "*" element = {<Error/>}/>
              <Route path = "/" element = {<Home/>}/>
              <Route path = "/login" element = {<Login/>}/>
              <Route path = "/register" element = {<Register/>}/>
              <Route path = "/commissions" element = {<Commission/>}/>
              <Route path = "/contacts" element = {<Contact/>}/>
              <Route path = "/tos" element = {<TOS/>}/>
            </Routes>
            :
            <Routes>
            <Route path = "*" element = {<Error/>}/>
            <Route path = "/" element = {<Home/>}/>
            <Route path = "/login" element = {<Login/>}/>
            <Route path = "/register" element = {<Register/>}/>
            <Route path = "/logout" element = {<Logout/>}/>
            <Route path = "/commissions" element = {<Commission/>}/>
            <Route path = "/contacts" element = {<Contact/>}/>
            <Route path = "/tos" element = {<TOS/>}/>
            <Route path = "/cart" element = {<Cart/>}/>
            <Route path = "/addtocart/:productId" element = {<AddToCart/>}/>
          </Routes>
          }
          <Footer/>
        </Router>
    </UserProvider>
  );
}

export default App;
