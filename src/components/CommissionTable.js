import '../App.css';
import {Table, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';

export default function StripedRowExample() {

  const {user, setUser} = useContext(UserContext);

  return (
    <Table>
      <thead>
        <tr>
          <th>Type</th>
          <th>Line art</th>
          <th>Flat Shading</th>
          <th>Rendered</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Portrait</td>
          <td>30 USD</td>
          <td>35 USD</td>
          <td>45 USD</td>
        </tr>
        <tr>
          <td>Half Body</td>
          <td>45 USD</td>
          <td>50 USD</td>
          <td>60 USD</td>
        </tr>
        <tr>
          <td>Full Body</td>
          <td>65 USD</td>
          <td>70 USD</td>
          <td>80 USD</td>
        </tr>
      </tbody>
    </Table>
  );
}