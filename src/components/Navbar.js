import {Container, Nav, Navbar} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import {Fragment, useContext} from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(
			<Navbar fixed="top" expand="lg" className = "navBar my-3 mx-5">
			      <Container>
			      	{
			      		(user.isAdmin)?
			      		<Fragment>
			      		<i class="bi bi-sliders2-vertical px-3"></i>
			        	<Navbar.Brand as = {NavLink} to = "/dashboard" className = "text-white">Dashboard</Navbar.Brand>
			        	</Fragment>
			      		:
			      		<Fragment>
			      		{
			      			(localStorage.getItem("token") !== null || user.isAdmin)
			      			?
			      				<Nav.Link as = {NavLink} to = "/cart" className = "text-white"><i class="bi bi-bag-heart"></i></Nav.Link>
			      			:
			      			     <p></p>
			      		}
						      		
			      		<Navbar.Brand as = {NavLink} to = "/" className = "text-white-50 mx-4">@_MinniJay_</Navbar.Brand>
			      		</Fragment>
			      	}
			        <Navbar.Toggle aria-controls="basic-navbar-nav" />
			        <Navbar.Collapse id="basic-navbar-nav">
	          			<Nav className="ms-auto">
					  			{
					  				(user.id !== null) ? 
						  				<Fragment>
						  					<Nav.Link className = "text-white">{user.email}</Nav.Link>
							            	<Nav.Link as= {NavLink} to ="/logout" className = "text-white">Logout</Nav.Link>
							            </Fragment>	
						            	:
							            <Fragment>
							            	<Nav.Link as = {NavLink} to ="/register" className = "text-white">Register</Nav.Link>
							            	<Nav.Link as = {NavLink} to ="/login" className = "text-white">Login</Nav.Link>
							            </Fragment>
					  			}
				          </Nav>
			        </Navbar.Collapse>
			      </Container>
			    </Navbar>
		);
}