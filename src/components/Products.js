import {useState, useEffect, useContext, Fragment} from 'react';
import {Button, Form} from "react-bootstrap";
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Products({productsProp}) {

	const [isActive, setisActive] = useState([]);
	const [isEditing, setIsEditing] = useState(false);
	const [product, setProduct] = useState([]);
	const [imageURL, setImageURL] = useState([]);
	const [description, setDescription] = useState([]);
	const [price, setPrice] = useState([]);

	const Toast = Swal.mixin({
	  toast: true,
	  position: 'top-end',
	  showConfirmButton: false,
	  timer: 3000,
	  timerProgressBar: true,
	  didOpen: (toast) => {
	    toast.addEventListener('mouseenter', Swal.stopTimer)
	    toast.addEventListener('mouseleave', Swal.resumeTimer)
	  }
	});

	useEffect(()=>{
		setisActive(productsProp.isActive);
	},[]);

	useEffect(()=>{
		
	},[isActive,isEditing]);

	function archiveProduct(event){
		       event.preventDefault();
		       	fetch(`${process.env.REACT_APP_URI}/products/archive/${productsProp._id}`, {
		       		method: "PATCH",
		       		headers:{
		       			Authorization: `Bearer ${localStorage.getItem("token")}`
		       		}
		       	}).then(response => response.json())
					.then(data =>{
						if(data === true){
							Toast.fire({
								title: `Product ${productsProp.name} Archived`,
								icon: "success"
							});
							setisActive(false);
						}else{
							Toast.fire({
								title: `Archiving ${productsProp.name} Failed`,
								icon: "error",
								text: data.err
							});
						}
					});
	}

	function restoreProduct(event){
		       event.preventDefault();
		       	fetch(`${process.env.REACT_APP_URI}/products/restore/${productsProp._id}`, {
		       		method: "PATCH",
		       		headers:{
		       			Authorization: `Bearer ${localStorage.getItem("token")}`
		       		}
		       	}).then(response => response.json())
					.then(data =>{
						if(data === true){
							Toast.fire({
								title: `Product ${productsProp.name} Restored`,
								icon: "success"
							});
							setisActive(true);
						}else{
							Toast.fire({
								title: `Restoration ${productsProp.name} Failed`,
								icon: "error",
								text: data.err
							});
						}
					});
	}

	function editProduct(productId){
			fetch(`${process.env.REACT_APP_URI}/products/update/${productId}`, {
				method: "PATCH",
				headers:{
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
						name: product,
						description: description,
						price: price,
						imageURL: imageURL
				})
			}).then(response => response.json())
			.then(data =>{
				if(data === true){
					Toast.fire({
						title: "Product Updated",
						icon: "success",
					})
				setIsEditing(false);
				productsProp.name = product;
				productsProp.description = description;
				productsProp.price = price;
				productsProp.imageURL = imageURL;
				}else{
					Toast.fire({
						title: "Product Updated Failed",
						icon: "error",
						text: data.err
					});
				}
			});
	}

    return (
    		<Fragment>
    		{
    			(isEditing)?
					<Fragment>
			    		<tr>
			    			<td><img src={productsProp.imageURL} class="dashboardImg img-fluid"/></td>
				    		<td>{productsProp._id}</td>
				    		<td>{productsProp.name}</td>
				    		<td>{productsProp.description}</td>
				    		<td>{productsProp.price}</td>
				    		<td>{productsProp.createdOn}</td>
				    		{
				    			(isActive)?
				    				<td>
				    				<Button onClick = {archiveProduct} variant="outline-danger my-3">Archive</Button>
				    				</td>
				    			:
				    				<td><Button onClick = {restoreProduct} variant="outline-success">Restore</Button></td>
				    		}			
			    		</tr>
			    		<tr>
			    			<td className ="col-4">
			    				<Form className = "border border-light p-3 text-center text-white">
			    				     <Form.Group className="mb-3" controlId="product">
			    				       <Form.Label>Product Name</Form.Label>
			    				       <Form.Control type="text" placeholder={productsProp.name} value = {product} onChange = {event => setProduct(event.target.value)} required/>
			    				     </Form.Group>
			    				     <Form.Group className="mb-3" controlId="description">
			    				       <Form.Label>Description</Form.Label>
			    				       <Form.Control type="text" placeholder={productsProp.description} value = {description} onChange = {event => setDescription(event.target.value)} required/>
			    				     </Form.Group>
			    				     <Form.Group className="mb-3" controlId="price">
			    				       <Form.Label>Price</Form.Label>
			    				       <Form.Control type="text" placeholder={productsProp.price} value = {price} onChange = {event => setPrice(event.target.value)} required/>
			    				     </Form.Group>
			    				     <Form.Group className="mb-3" controlId="imageURL">
			    				       <Form.Label>Product Image URL</Form.Label>
			    				       <Form.Control type="text" placeholder={productsProp.imageURL} value = {imageURL} onChange = {event => setImageURL(event.target.value)} required/>
			    				     </Form.Group>
			    				     <Button onClick={() => editProduct(productsProp._id)} variant="outline-success mx-3">Save</Button>
			    				     <Button onClick={() => setIsEditing(false)} variant="outline-danger mx-3">Cancel</Button>
			    				</Form>
			    			</td>	
			    		</tr>
					</Fragment>
    			:
		    		<tr>
		    			<td><img src={productsProp.imageURL} class="dashboardImg img-fluid"/></td>
			    		<td>{productsProp._id}</td>
			    		<td>{productsProp.name}</td>
			    		<td>{productsProp.description}</td>
			    		<td>{productsProp.price}</td>
			    		<td>{productsProp.createdOn}</td>
			    		{
			    			(isActive)?
			    				<td>
			    				<Button onClick = {archiveProduct} variant="outline-danger my-3">Archive</Button>
			    				<Button onClick={() => setIsEditing(true)} variant="outline-success">Edit</Button>
			    				</td>
			    			:
			    				<td>
			    				<Button onClick = {restoreProduct} variant="outline-success">Restore</Button>
			    				<Button onClick={() => setIsEditing(true)} variant="outline-success">Edit</Button>
			    				</td>
			    		}			
		    		</tr>
    		}
			</Fragment>   		
    	)
}