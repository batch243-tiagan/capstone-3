import Button from 'react-bootstrap/Button';
import {Row, Col, Container, Form, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, useParams, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import InputGroup from 'react-bootstrap/InputGroup'

import UserContext from '../UserContext';

export default function Login(){

	const {user, setUser} = useContext(UserContext);
	const [highlights, setHighlights] = useState([]);
	const [orders, setOrders] = useState([]);
	const [totalAmount, setTotalAmount] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [productPrice, setProductPrice] = useState(0);
	const [order, setOrder] = useState([]);
	let { productId } = useParams();

	const Toast = Swal.mixin({
	  toast: true,
	  position: 'top-end',
	  showConfirmButton: false,
	  timer: 3000,
	  timerProgressBar: true,
	  didOpen: (toast) => {
	    toast.addEventListener('mouseenter', Swal.stopTimer)
	    toast.addEventListener('mouseleave', Swal.resumeTimer)
	  }
	});

	useEffect(()=>{
		displayProduct();
		getOrders();
	},[])

	function displayProduct(){
		fetch(`${process.env.REACT_APP_URI}/products/get/${productId}`,
			{headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
		.then(response => response.json())
		.then(data =>{
			setProductPrice(data.price);
			setHighlights(
					<Card className = "commissionCardImg bg-dark text-white p-3 h-100 text-center">
						<div className = "h-100">
							<Card.Img variant="top" src={data.imageURL} />
						</div>
					    <Card.Body>
					        <Card.Title>
					        	<h2>{data.name}</h2>
					        </Card.Title>
					        <Card.Text>
					          	{data.description}
					        </Card.Text>
					        <Card.Text>
					          	Php {data.price} 
					        </Card.Text>
					    </Card.Body>
					</Card>
			)
		})
	}

	function createOrderIfEmpty(){
		fetch(`${process.env.REACT_APP_URI}/orders/create`,
			{
				method: "POST",
				headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
		.then(response => response.json())
		.then(order =>{
			if(order === true){
				Toast.fire({
				  	title: "New Order created",
					icon: "success"
				})
				displayProduct();
			}else{
				Toast.fire({
				  title: "Order was not created",
					icon: "error",
					text: order.err
				})
			}
		});
	}

	function getOrders(){
		fetch(`${process.env.REACT_APP_URI}/orders/getAllOfUser`,
			{headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
		.then(response => response.json())
		.then(orders =>{
			setOrders(orders.map(order => {
				if(order.purchasedOn === undefined){
					return(
							<option value={order._id}>{order._id}</option>
						)
				}
			}));
		})
	}

	function add(event){
		event.preventDefault();
		setQuantity(quantity + 1);
		setTotalAmount(productPrice * (quantity + 1));
	}

	function subtract(event){
		event.preventDefault();
		if(quantity > 0){
			setQuantity(quantity - 1);
			setTotalAmount(productPrice * (quantity - 1));
		}
	}

	function makeOrder(productId){
		fetch(`${process.env.REACT_APP_URI}/orders/addProduct/${order}`, {
			method: "PATCH",
			headers:{
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
					productId : productId,
					quantity : quantity + 1
			})
		}).then(response => response.json())
		.then(data =>{
			if(data === true){
				Toast.fire({
				  	title: "Product Added To Cart",
					icon: "success"
				})
				displayProduct();
			}else{
				Toast.fire({
				  title: "Product Was not added to Cart",
					icon: "error",
					text: data.err
				})
			}
		});
	}

	return(

			<Container className = "homeContainer pt-5">
			<Row className = "backButtonRow">
			 	<Col>
			 		<div>
			 			<Button as = {Link} to = "/commissions" variant="outline-light" className = "rounded-pill col-1 my-5">Back</Button>
			 		</div>
			 	</Col>
			</Row>
			<Row className = "w-100">
				<Col className = "mb-4 col-4 offset-3">
					{highlights}
				</Col>
				<Col className = "mb-4 col-2 ">
					<Form className = "text-white">
					      <Form.Group className="my-3" controlId="quantity">
					        <Form.Label>Quantity</Form.Label>
					        <Form.Control value = {quantity} disabled readOnly readtype="number" placeholder="0" />
					      </Form.Group>
					      <div className="text-center">
					      <Button onClick = {subtract} variant="success mx-3" type="submit">
					        <i class="bi bi-dash"></i>
					      </Button>
					      	<Button onClick = {add} variant="success mx-3" type="submit">
					      	  <i class="bi bi-plus"></i>
					      	</Button>
					      </div>
					      <Form.Label className="my-3">Total Amount</Form.Label>
					      <InputGroup  controlId="totalAmount">
					              <InputGroup.Text>Php</InputGroup.Text>
					              <Form.Control value = {totalAmount} disabled readOnly type="number" placeholder="0" />
					       </InputGroup>
					      <Form.Group className="my-3" controlId="formBasicSelect">
					              <Form.Label>Select an Order Listing</Form.Label>
					              <Form.Control
					                as="select"
					                value={order}
					                onChange={e => {
					                  setOrder(e.target.value);
					                }}
					              >
					              <option>Open this select menu</option>
					                {orders}
					              </Form.Control>
					            </Form.Group>
					      <div className="text-center my-3">
					      	<Button onClick={() => makeOrder(productId)} variant="success">
					        	Add To Cart
					      	</Button>
					      </div>      
					    </Form>
				</Col>
			</Row>
			</Container>
		)
}