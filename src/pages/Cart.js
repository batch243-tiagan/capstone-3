import {Container, Row, Col, Button,Table,Form, Dropdown} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import {useState, useEffect, useContext, Fragment} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Cart(){
	const {user, setUser} = useContext(UserContext);
	const [orders, setOrders] = useState([]);
	const [retrievedProduct, setRetrievedProduct] = useState([]);
	const [name, setName] = useState([]);

	const Toast = Swal.mixin({
	  toast: true,
	  position: 'top-end',
	  showConfirmButton: false,
	  timer: 3000,
	  timerProgressBar: true,
	  didOpen: (toast) => {
	    toast.addEventListener('mouseenter', Swal.stopTimer)
	    toast.addEventListener('mouseleave', Swal.resumeTimer)
	  }
	});

	function displayProduct(){
		fetch(`${process.env.REACT_APP_URI}/orders/getAllOfUser`,
			{headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
		.then(response => response.json())
		.then(orders =>{
				setOrders(orders.map(order => {
					return(
							<Fragment>
								<Table key = {order._id}>
								      <thead>
								        <tr>
								          <th>Product Preview</th>
								          <th>Name</th>
								          <th>Description</th>
								          <th>Price</th>
								          <th>Quantity</th>
								        </tr>
								      </thead>
								      <tbody>
								      	{
						      				order.products.map(product =>{
						      					return(
				      								<tr>
				      									<td><img src={product.imageURL} class="dashboardImg img-fluid"/></td>
				      								    <th>{product.name}</th>
				      								    <th>{product.description}</th>
				      								    <th>Php {product.price}</th>
				      								    <th>
				      								    {
				      								    	(order.purchasedOn !== undefined)?
				      								    	<p className = "d-inline">{product.quantity}</p>
				      								    	:
				      								    	<Fragment>
				      								    		<Button onClick={() => changeQuantity(product, order._id, -1)} variant="border border-danger text-white mx-2" type="submit">
				      								    		  <i class="bi bi-dash"></i>
				      								    		</Button>
				      								    		<p className = "d-inline">{product.quantity}</p>
				      								    		<Button onClick={() => changeQuantity(product, order._id, 1)} variant="border border-success text-white mx-2" type="submit">
				      								    		  <i class="bi bi-plus"></i>
				      								    		</Button>
				      								    	</Fragment>
				      								    }
				      								    </th>
				      							  	</tr>
						      					)	
						      		       })
								      	}	
								      	<tr>
				      			      		<td>Order Id: {order._id}</td>
				      			      		<td></td>
				      			      		<td></td>
				      			      		{
				      			      			(order.purchasedOn === undefined)?
				      			      				<td>Total Amount to Pay: Php {order.amountToPay}</td>
				      			      			:
				      			      				<td>Total Amount Payed: Php {order.amountToPay}</td>
				      			      		}
				      			      		
								      		<td>
							      				{
	      						      				(order.purchasedOn === undefined)?
	      					      						<Button onClick={() => finalizeOrder(order._id)} variant="outline-success"><i class="bi bi-plus-square-dotted"></i>
	      					      					     	Checkout
	      					      					    </Button>
	      						      				:
      						      						<Button disabled variant="outline-success">
      						      					     	Purchased On: {order.purchasedOn}
      						      					    </Button>
							      				}
								      		</td>
								      	</tr>
								      </tbody>				        
							    </Table>
							    <p className ="text-white text-center m-5">----Check Items Before Checking Out----</p>
							</Fragment>
					)
				}));	
		})
	}

	function changeQuantity(product, orderId, quantity){
		if(product.quantity >= 1){
			fetch(`${process.env.REACT_APP_URI}/orders/addProduct/${orderId}`, {
			method: "PATCH",
			headers:{
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
					productId : product.productId,
					quantity : quantity
			})
		}).then(response => response.json())
		.then(data =>{
			if(data === true && quantity === 1){
				Toast.fire({
				  	title: "Quantity Increased",
					icon: "success"
				})
				displayProduct();
			}else if(data === true && quantity === -1){
				Toast.fire({
				  	title: "Quantity Decreased",
					icon: "success"
				})
				displayProduct();
			}else{
				Toast.fire({
				  title: "Product Was not added to Cart",
					icon: "error",
					text: data.err
				})
			}
		});
		}else{
			Toast.fire({
				  title: "Wooosshhh",
					icon: "error",
					text: "Imagine na lang na delete"
				})
		}
	}

	function creatOrder(){
		fetch(`${process.env.REACT_APP_URI}/orders/create`,
			{
				method: "POST",
				headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
		.then(response => response.json())
		.then(order =>{
			if(order === true){
				Toast.fire({
				  	title: "New Order created",
					icon: "success"
				})
				displayProduct();
			}else{
				Toast.fire({
				  title: "Order was not created",
					icon: "error",
					text: order.err
				})
			}
		});
	}

	function finalizeOrder(orderId){
		fetch(`${process.env.REACT_APP_URI}/orders/makePurchase/${orderId}`,
			{
				method: "PATCH",
				headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
		.then(response => response.json())
		.then(order =>{
			if(order === true){
				Toast.fire({
				  	title: "Order has been purchased",
					icon: "success"
				})
				displayProduct();
			}else{
				Toast.fire({
				  title: "There was a problem purchasing the order",
					icon: "error",
					text: order.err
				})
			}
		});
	}

	useEffect(()=>{
		displayProduct();
	},[])

	return(
		<Container className = "homeContainer">
			<Row className = "PaddingRow">
			 	<Col>
			 		
			 	</Col>
			</Row>
			<Row className = "backButtonRow">
			 	<Col>
			 		<div>
			 			<Button as = {Link} to = "/" variant="outline-light" className = "rounded-pill col-1">Back</Button>
			 		</div>
			 	</Col>
			</Row>
			<Row className = "commissionTitle">
				<Col className = "commissionTitleCol text-center">
				 	<h1 className = "pt-5 pb-3 text-white d-inline-block"> ORDER LIST </h1>
				 	<Button onClick={creatOrder} variant="outline-success" className = "d-inline"><i class="bi bi-bag-plus-fill"></i></Button>
				 	<hr className ="solid col-2 offset-5"/>
				</Col>
			</Row>
			<Row>
				<Col className = "dashboardCol mt-5 col-10 offset-1">
			      	<Fragment>
			      		{orders}
			      	</Fragment>
				</Col>
			</Row>
		</Container>
	);
}