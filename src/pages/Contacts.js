import '../App.css';
import {Row, Col, Table,Container, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';

export default function Contacts() {

  const {user, setUser} = useContext(UserContext);

  return (
    <Container className = "homeContainer">
      <Row className = "PaddingRow">
        <Col>
      
        </Col>
      </Row>
      <Row className = "backButtonRow">
          <Col>
            <div>
              <Button as = {Link} to = "/" variant="outline-light" className = "rounded-pill col-1">Back</Button>
            </div>
          </Col>
        </Row>
      <Row className = "commissionTitle">
        <Col className = "commissionTitleCol">
          <h1 className = "pt-5 pb-3 text-white"> CONTACTS DETAILS </h1>
          <hr className ="solid col-2 offset-5"/>
        </Col>
      </Row>
      <Row className = "commissionTable">
          <Col className = "commissionTableCol col-6 offset-3">
            <Table className = "text-white text-start">
              <thead className = "m-3">
                      <tr >
                        <td className = "col-2">Site</td>
                        <td className = "col-10">Tag</td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td className = "col-2">Discord</td>
                        <td className = "col-10">MinniJay#3790</td>
                      </tr>
                      <tr>
                        <td className = "col-2">Twitter</td>
                        <td className = "col-10">_MinniJay_</td>
                      </tr>
                      <tr>
                        <td className = "col-2">Gmail</td>
                        <td className = "col-10">minimeminnijay@gmail.com</td>
                      </tr>
                      <tr>
                        <td className = "col-2">Ko-fi</td>
                        <td className = "col-10">https://ko-fi.com/minnijay3790/shop</td>
                      </tr>
                    </tbody>
          </Table>
        </Col>
      </Row>
      <Row className = "additionalInfo mt-4">
          <Col className = "additionalInfoCol col-8 offset-2">
            <hr className ="solid mt-4 col-6 offset-3"/>
              <p className = "pt-4 pb-3">I PREFER AND REPLY FASTEST ON DISCROD</p>
            <hr className ="solid mt-4 col-6 offset-3"/>
          </Col>
        </Row>
    </Container>
  );
}