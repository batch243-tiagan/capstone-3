import '../App.css';
import Container from 'react-bootstrap/Container'
import Products from '../components/Products';
import {Row, Col, Button,Table,Form} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {React, useState, useEffect, Fragment, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'


export default function Commission() {

	const {user, setUser} = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const [product, setProduct] = useState([]);
	const [imageURL, setImageURL] = useState([]);
	const [description, setDescription] = useState([]);
	const [price, setPrice] = useState([]);

	const Toast = Swal.mixin({
	  toast: true,
	  position: 'top-end',
	  showConfirmButton: false,
	  timer: 3000,
	  timerProgressBar: true,
	  didOpen: (toast) => {
	    toast.addEventListener('mouseenter', Swal.stopTimer)
	    toast.addEventListener('mouseleave', Swal.resumeTimer)
	  }
	});

	const [addFormActive, setAddFormActive] = useState(false);

	useEffect(()=>{
		displayProduct();
	},[]);

	function displayProduct(){
		fetch(`${process.env.REACT_APP_URI}/products/getAll`,
			{headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}}).then(response => response.json())
		.then(data =>{
			setProducts(data.map(products => {
		return(
				<Products key = {products._id} productsProp = {products}/>
			)
			}))
		})
	}

	function addProduct(event){
       event.preventDefault();
       	fetch(`${process.env.REACT_APP_URI}/products/create`, {
       		method: "POST",
       		headers:{
       			'Content-Type' : 'application/json',
       			Authorization: `Bearer ${localStorage.getItem("token")}`
       		},
       		body: JSON.stringify({
       				name: product,
       				description: description,
       				price: price,
       				imageURL: imageURL
       		})
       	}).then(response => response.json())
			.then(data =>{
				console.log(data);
				if(data === true){
					Toast.fire({
					  	title: "Product Created",
						icon: "success"
					})
					displayProduct();
					setProduct('');
					setDescription('');
					setPrice('');
					setImageURL('');
				}else{
					Toast.fire({
					  title: "Product Creation Failed",
						icon: "error",
						text: data.err
					})
				}
			});
  	}

  	function modal(){
  		setAddFormActive(!addFormActive);
  	}

	return(
		(!user.isAdmin)?
		<Navigate to = "/"/>
		:
		<Container className = "dashboardContainer">
			<Row className = "PaddingRow">
			 	<Col>
			 		
			 	</Col>
			</Row>
			<Row>
				<Col className = "dashboardCol mt-5 col-10 offset-1">
					<Table>
				      <thead>
				        <tr>
				          <th>Product Image</th>
				          <th>Product Id</th>
				          <th>Product Name</th>
				          <th>Description</th>
				          <th>Price</th>
				          <th>Date Created</th>
				          <th>Edit</th>
				        </tr>
				      </thead>				       
				      <tbody>
			        	<Fragment>
			        		{products}
			        	</Fragment>
	        	    		<tr>
	        		    		<td></td>
	        	    		</tr>
				      </tbody>  
				    </Table>
				</Col>
			</Row>
			<Row className = "w-100">
				<Col className = "col-md-4 col-8 mx-auto my-5 text-center">
					{
							(addFormActive)?
							<Form className = "border border-light p-3 text-center text-white">
							     <div className ="text-end">
							     	<Button onClick = {modal} variant="outline-danger"><i class="bi bi-x-circle"></i></Button>
							     </div>
							     <Form.Group className="mb-3" controlId="product">
							       <Form.Label>Product Name</Form.Label>
							       <Form.Control type="text" placeholder="Enter Product Name" value = {product} onChange = {event => setProduct(event.target.value)} required/>
							     </Form.Group>
							     <Form.Group className="mb-3" controlId="description">
							       <Form.Label>Description</Form.Label>
							       <Form.Control type="text" placeholder="Enter Product Description" value = {description} onChange = {event => setDescription(event.target.value)} required/>
							     </Form.Group>
							     <Form.Group className="mb-3" controlId="price">
							       <Form.Label>Price</Form.Label>
							       <Form.Control type="text" placeholder="Enter Product Price" value = {price} onChange = {event => setPrice(event.target.value)} required/>
							     </Form.Group>
							     <Form.Group className="mb-3" controlId="imageURL">
							       <Form.Label>Product Image URL</Form.Label>
							       <Form.Control type="text" placeholder="https://via.placeholder.com/100" value = {imageURL} onChange = {event => setImageURL(event.target.value)} required/>
							     </Form.Group>

							     <Button onClick = {addProduct} variant="outline-success mx-5"><i class="bi bi-plus-square-dotted"></i>
							     	Add
							     </Button>
							</Form>
						    :
					    	<Button onClick = {modal} variant="outline-success"><i class="bi bi-plus-square-dotted"></i>
					         	Add Product
					        </Button>
					}
				</Col>
			</Row>

		</Container>
	);
}

