import '../App.css';
import Container from 'react-bootstrap/Container';
import {Row, Col, Button} from 'react-bootstrap';
import ProfileIcon from '../components/ProfileIcon'
import {Link} from 'react-router-dom';


export default function Home(){
	return(
			<Container className = "homeContainer">
				<Row className = "PaddingRow">
				 	<Col>
				 		
				 	</Col>
				</Row>
				<Row className = "profileIconRow">
				 	<Col className = "pt-5">
				 		<ProfileIcon/>
				 	</Col>
				</Row>
				<Row className = "introductionRow p-3 text-white">
				 	<h1 className = "pb-5"> MINNIJAY </h1>
				 	<hr className ="solid col-2 offset-5"/>
				 	<p className = "col-6 offset-3 p-1 text-white">
				 		Heyo, It's MinniJay
				 	</p>
				 	<p className = "col-6 offset-3 p-1 text-white">
				 		I'm an aspiring Freelance Illustrator.
				 		Thanks for dropping by and I hope you support my work It would help alot!
				 	</p>
				 	<p className = "col-6 offset-3 p-1 text-white">
				 		If you have any questions, feel free to contact me through my socials.
				 		Pardon for the lack of introduction I'm not much of a talker.
				 	</p>
				</Row>
				<Row className = "navigationRow pb-2">
				 	<hr className ="solid col-6 offset-3"/>
				 	<div>
				 		<Button as = {Link} to = "/contacts" variant="outline-light" className = "navigationRowButton col-2">Contact</Button>
				 		<Button as = {Link} to = "/commissions" variant="outline-light" className = "navigationRowButton col-2" >Commision</Button>
				 	</div>
				 	<div className ="pb-2">
				 		<Button as = {Link} to = "/tos" variant="outline-light" className = "navigationRowButton col-2">TOS</Button>
				 	</div>
				</Row>
			</Container>
		)
}