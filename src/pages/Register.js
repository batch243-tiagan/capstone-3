import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Row, Col, Container} from 'react-bootstrap';
import Swal from 'sweetalert2'
import {useNavigate} from 'react-router-dom';
import {useState, useEffect,useContext} from 'react';
import UserContext from '../UserContext';

export default function Register(){

	const {user, setUser} = useContext(UserContext);
	const history = useNavigate();

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isEnabled, setIsEnabled] = useState(false);

	useEffect(() =>{
		localStorage.removeItem("token");
	});

	useEffect(() =>{
		if((email === '' || password1 === '' || password2 === '') 
			|| (password1 !== password2)){
			setIsEnabled(true);
		}else{
			setIsEnabled(false);
		}
	}, [email, password1, password2]);

	function registerUser(event){
		event.preventDefault();
			fetch(`${process.env.REACT_APP_URI}/users/register`, {
				method: "POST",
				headers:{
					'Content-Type' : 'application/json',
				},
				body: JSON.stringify({
						email: email,
						password: password1
				})
			}).then(response => response.json())
			.then(data =>{
				if(data === true){
					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "Welcome to our website!"
					}).then((res) =>{
						if(res){
							loginUser();
						}
					});	
				}else{
					Swal.fire({
						title: "Registration Failed",
						icon: "error",
						text: data.err
					});
				}
			});
	}

	function loginUser(){
		fetch(`${process.env.REACT_APP_URI}/users/login`, {
			method: "POST",
			headers:{
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
					email: email,
					password: password1
			})
		}).then(response => response.json())
		.then(data =>{
			if(data.accessToken !== undefined){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				history("/");
			}else{
				Swal.fire({
					title: `Login Failed`,
					icon: "error",
					text: data.err
				});
			}
		});
	}

	const retrieveUserDetails = (token) =>{
			fetch(`${process.env.REACT_APP_URI}/users/getUserDetail`,
				{headers: {
					Authorization: `Bearer ${token}`
				}}).then(response => response.json())
			.then(data => {
				setUser({id:data._id, email:data.email, isAdmin:data.isAdmin});
			});
		}

	return(
		<Container className = "homeContainer pt-5">
			<Row className = "PaddingRow">
			 	<Col>
			 		
			 	</Col>
			</Row>
			<Row className = "w-100">
				<Col className = "col-md-4 col-8 mx-auto my-5">
					<Form onSubmit = {registerUser} className = "border border-light text-white text-center p-3">
					     <Form.Group className="mb-3" controlId="email">
					       <Form.Label>Email address</Form.Label>
					       <Form.Control type="email" placeholder="Enter Email" value = {email} onChange = {event => setEmail(event.target.value)} required/>
					     </Form.Group>
					     <Form.Group className="mb-3" controlId="password1">
					       <Form.Label>Password</Form.Label>
					       <Form.Control type="password" placeholder="Password" value = {password1} onChange = {event => setPassword1(event.target.value)} required/>
					     </Form.Group>
					     <Form.Group className="mb-3" controlId="password2">
					       <Form.Label>Verify Password</Form.Label>
					       <Form.Control type="password" placeholder="Password" value = {password2} onChange = {event => setPassword2(event.target.value)} required/>
					     </Form.Group>
					     <Button variant= "border border-primary text-white" disabled = {isEnabled} type="submit">
					       Register
					     </Button>
					</Form>
				</Col>
			</Row>
		</Container>
		)
}