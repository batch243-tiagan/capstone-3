import '../App.css';
import {Row, Col, Table,Container, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';

export default function TOS() {

  const {user, setUser} = useContext(UserContext);

  return (
    <Container className = "homeContainer">
      <Row className = "PaddingRow">
        <Col>
      
        </Col>
      </Row>
      <Row className = "backButtonRow">
          <Col>
            <div>
              <Button as = {Link} to = "/" variant="outline-light" className = "rounded-pill col-1">Back</Button>
            </div>
          </Col>
        </Row>
      <Row className = "commissionTitle">
        <Col className = "commissionTitleCol">
          <h1 className = "pt-5 pb-3 text-white"> TERMS OF SERVICE </h1>
          <hr className ="solid col-2 offset-5"/>
        </Col>
      </Row>
      <Row className = "additionalInfo mt-4">
          <Col className = "tosInfoCol col-8 offset-2">
            <hr className ="solid mt-4 col-6 offset-3"/>
              <h1 className = "pt-4 pb-3">Payment</h1>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                Payment will be made via Paypal, I will try and send an invoice to make things easier to manage.
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                Commissions will be paid half when the commission is accepted and half before I hand over the final piece.
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                I won't be giving out refunds if I already started the lineart or if I have already made significant work into the sketch especially for more detailed pieces.
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                I don't mind minor changes or fixes but major changes during the lineart and rendering process may incur additional fees.
              </p>
            </div>
          </Col>
          <Col className = "tosInfoCol col-8 offset-2">
            <hr className ="solid mt-4 col-6 offset-3"/>
              <h1 className = "pt-4 pb-3">Notes</h1>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                I have the right to decline commissions due to certain circumstance (i.e sudden irl issues) or If I don't feel confident enought to take on the requested piece.
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                I draw both male and female characters but do note that females are my stronger point
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                I don't mind complicated designs as long as there is a substantial amount of reference provided.
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                I don't draw NSFW, Gore, Furries, Mechas.
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                References and a solid understanding of what you want the final piece would look like would be highly appreciated, the more vague the requested piece the more likely it would take more time to make.
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
                Please feel free to revise and suggest things that you want in the commissioned piece especially during the sketch phase it will be easier for the both of us in the long run.
              </p>
            </div>
          </Col>
          <Col className = "tosInfoCol col-8 offset-2">
            <hr className ="solid mt-4 col-6 offset-3"/>
              <h1 className = "pt-4 pb-3">COPYRIGHT AND REPOSTING</h1>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
               I own the rights to my work unless stated otherwise
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
               I can repost and use pieces that I have the rights to for promotional uses
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
               The use of my work for commercial use is not allowed unless you commissioned a piece for commercial use
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
               The commissioner is allowed to use, edit, repost, and print the commissioned piece if it's for personal use
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
               The commissioner is not allowed to claim the piece as their own
              </p>
            </div>
            <div className = "col-8 offset-2 my-3">
              <i class="bi bi-chevron-right d-inline"></i>
              <p className = "mx-4 pt-1 d-inline">
               Credit is not required but really appriciated
              </p>
            </div>
          </Col>
        </Row>
      <Row className = "additionalInfo mt-4">
          <Col className = "tosCol col-8 offset-2">
            <hr className ="solid mt-4 col-6 offset-3"/>
              <h1 className = "pt-4 pb-3">ENDING NOTE</h1>
            <hr className ="solid mt-4 col-6 offset-3"/>
            <p className = "col-8 offset-2 p-1">
              I'm not omnipotent so the things stated above isn't set in stone I still have the right to agree or disagree to certain things I find improper or unfair so please avoid being that guy and find loopholes or things like that let's be civilized and to put it bluntly not be dicks to each other
            </p>
          </Col>
        </Row>
    </Container>
  );
}